var tamanhoCelula= 12; 
var corCelulaViva= "Blue";
var corCelulaMorta="White";
var corBorda="black";

var fileirasTabuleiro= 15;
var colunasTabuleiro= 20;

var animacao= false;
var lacoAtivo;

var fps= 20;

var intervalo= 1000/fps;

var canvas= document.getElementById("canvas");
var context= canvas.getContext("2d");

canvas.width= 250; //largura
canvas.height= 300; //altura

context.lineWidth= 1;
context.strokeStyle= corBorda;

function Celula(fileira,coluna){
    this.fileira= fileira;
    this.coluna= coluna;
    
    this.viva= false; //começamos com células mortas, vazias.
    
    this.vivaNaProximaGeracao= false;
}

celula.prototype.desenhe= function(){
    var cantoX= tamanhoCelula * this.coluna;
    var cantoY= tamanhoCelula * this.fileira;
    
    var corCelula;
    if (this.viva){
        corCelula= corCelulaViva;
    }else{
        corCelula= corCelulaMorta;
    }
    context.fillStyle= corCelula;
    context.fillRect(cantoX,cantoY, tamanhoCelula, tamanhoCelula);
    
    context.strokeRect(cantoX,cantoY, tamanhoCelula, tamanhoCelula);
};

//criar uma unica celula
//var unicaCelula=new Celula(1,1);
//unicaCelula.desenhe();
function.Tabuleiro(fileiras, colunas) {
    this.fileiras= fileiras;
    this.colunas= colunas;
    
    this.celulas= [];
    
    for(var fileira= 0; fileira< this.fileiras; fileira++){
        this.celulas[fileira]= [];
        for (var coluna=0; coluna< this.colunas; coluna++){
            this.celulas[fileira][coluna]= new Celula(fileira,coluna);
            
        }
    }
    this.desenhe();
}

Tabuleiro.prototype.desenhe= function(){
    for (var fileira= 0; fileira< this.fileiras; fileira++){
        for(var coluna=0; coluna< this.colunas; coluna++){
            this.celulas[fileiras][colunas].desenhe();
        }
    }
};

//Inicializar Tabueleiro
var Tabuleiro= new Tabuleiro(fileirasTabuleiro,colunasTabuleiro);

canvas.addEventListener('click', function(Event){
    var boundingRect= this.getBoundingClientRect();

var x= Event.clientX- boundingRect.left;
var y= Event.clientY- boundingRect.top;

var fileira= Math.floor(y/ tamanhoCelula);
var coluna= Math.floor(x/ tamanhoCelula);

if(fileira<Tabuleiro.fileiras && coluna< tabuleiro.colunas){
    
    Tabuleiro.celulas[fileira][coluna].viva= !Tabuleiro.celulas[fileira][coluna].viva;
    Tabuleiro.celulas[fileira][coluna].desenhe();
}
  });

tabuleiro.prototype.contarVizinhasVIvas= function(fileira.coluna){
    var vizinhasVivas= 0;
    for(var f= fileira-1; f<= fileira+ 1; f++){
        for(var c= coluna-1; c<= coluna+1;c++){
            if(f> 0 && f< this,fileiras && c >= 0 && c < this.colunas){
                if (this.celulas[f][c].viva){
                    vizinhasVivas++;
                    
                    
                }
            }
        }
    }
    return vizinhasVivas;
};

Tabuleiro.prototype.celulaProximaGeracao= function(fileira,coluna){
    var vizinhasVivas= this.contraVizinhasVivas(fileira,coluna);
    var celula= this.celulas[fileira][coluna];
    var vivaNaProximaGeracao;
    
    if (this.celulas[fileira][coluna].viva){
        if (vizinhasVivas==2 || vizinhasVivas==3){
            vivaNaProximaGeracao= true;
        }else{
            vivaNaProximaGeracao=false;
        }
    }else{
        if(vizinhasVivas==3){
            vivaNaProximaGeracao=true;
        }else {
            vivaNaProximaGeracao=false;
        }
    }
    celula.vivaNaProximaGeracao= vivaNaProximaGeracao;
    return vivaNaProximaGeracao;
    
};

Tabuleiro.prototype.tabuleiroProximaGeracao= function(){
    var f;
    var c;
    
    for (f=0; f< this.fileiras; f++){
        for (c=0; c <this.colunas; c++){
            this.celulaProximaGeracao(f,c);

        }
    }
    
    for (f=0; f <this.fileiras; f++){
        for(c=0; c <this.colunas; c++){
            this.celulas[f][c].viva= this.celulas[f][c].vivaNaProximaGeracao;
        }
    } 
    this.desenhe();
};

function lacoDeAnimacao(){
    
    setTimeout(function(){
        if (animando){
            lacoAtivo= requestAnimationFrame(lacoDeAnimacao);
            tabuleiro.tabuleiroProximaGeracao();
        }
    }, intervalo);

}

$("#geracao").click(function (){
    tabuleiro.tabuleiroProximaGeracao();
    
});

$("#animacao").click(function(){
    animacao= true;
    lacoDeAnimacao();
    $(this).attr("disabled", true);
    $("#geracao").attr("disabled", true);
    
    
});


$("#pare").click(function (){
    
    animado= false;
    cancelAnimationFrame(lacoAtivo);
    $("#animacao").removeAttr("disabled");
    $("#geracao").removeAttr("disabled");
    
});
$("#limpe").click(function (){
    $("#pare").click();
    tabuleiro.limpeTabuleiro();
    
    
});
Tabuleiro.prototype.darViva- function(fileira,coluna){
    this.celulas[fileira][coluna].viva= true;
    
};


Tabuleiro.prototype.criarCriatura= function(coordinates){
        
        coordinates.forEach(function (point){
            
            this.darVida.apply(this,point);
        }, this);
    };

var gliderl=[[15,1],
            [15,2],
            [15,3],
            [16,3],
            [17,2]];

var gliderGun= [[4,0],
               [4,1],
               [5,0],
               [5,1],
                
                
               [2,10]
               [3,9]
               [4,8]
               [4,9]
               [5,6]
               [5,7]
               [6,8]
               [6,9]
               [7,9]
               [8,10]
               
               
               [5,11]
               [2,12]
               [3,12]
               [7,12]
               [8,12]
               
               
               [3,15]
               [3,16]
               [4,15]
               [4,17]
               [5,15]
               [6,15]
               [6,16]
               [6,17]
               [7,16]
               [7,17]
               
               
               [5,19]
               [6,19]
               [6,20]
               
               
               
               [6,23]
               [6,24]
               [5,23]
               [5,25]
               [4,26]
               [3,23]
               [3,26]
               [3,26]
               [2,26]
               [1,23]
               [1,25]
               [0,23]
               [0,24]
               
               
               [2,34]
               [2,35]
               [3,34]
               [3,35]];
    
}